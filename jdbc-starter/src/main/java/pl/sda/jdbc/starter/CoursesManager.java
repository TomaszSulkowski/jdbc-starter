package pl.sda.jdbc.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

public class CoursesManager {

    private static Logger LOG = LoggerFactory.getLogger(CoursesManager.class);


    public void createCoursesTable() throws SQLException {
        LOG.info("createCoursesTable method init");
        ConnectionFactory factory = new ConnectionFactory("/sda_courses.properties");
        try (Connection connection = ((ConnectionFactory) factory).getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.courses (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  place VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  start_date DATE NOT NULL,\n" +
                    "  end_date DATE NOT NULL,\n" +
                    "PRIMARY KEY (id))\n");

            LOG.info("Table sda academy courses created!");


//            addCourse("JavaKTW18", "Katowice", LocalDate.of(2019,6,1)),
//                    LocalDate.of(2019,6,1);
        } catch (SQLException e) {


        }
    }

    public void createStudentTable() throws SQLException {
        LOG.info("createStudentTable method init");
        ConnectionFactory factory = new ConnectionFactory("/sda_courses.properties");
        try (Connection connection = ((ConnectionFactory) factory).getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.students (\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  name VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  description VARCHAR(200) DEFAULT NULL,\n" +
                    "  seat VARCHAR(10) DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");

            LOG.info("Table sda academy students created!");


//            addCourse("JavaKTW18", "Katowice", LocalDate.of(2019,6,1)),
//                    LocalDate.of(2019,6,1);
        } catch (SQLException e) {


        }
    }


    public void createAttendanceListTable() throws SQLException {
        LOG.info("createStudentTable method init");
        ConnectionFactory factory = new ConnectionFactory("/sda_courses.properties");
        try (Connection connection = ((ConnectionFactory) factory).getConnection()) {
            LOG.info("Connected");
            Statement statement = connection.createStatement();

            statement.executeUpdate("CREATE TABLE IF NOT EXISTS sda_courses.attendance_list(\n" +
                    "  id INT NOT NULL AUTO_INCREMENT,\n" +
                    "  student_id INT DEFAULT NULL,\n" +
                    "  course_id INT DEFAULT NULL,\n" +
                    "  date DATETIME DEFAULT NULL,\n" +
                    "PRIMARY KEY (id),\n" +
                    "FOREIGN KEY (student_id) REFERENCES sda_courses.students(id),\n" +
                    "FOREIGN KEY (course_id) REFERENCES sda_courses.courses(id))\n");

            LOG.info("Table sda academy attendents created!");


//            addCourse("JavaKTW18", "Katowice", LocalDate.of(2019,6,1)),
//                    LocalDate.of(2019,6,1);
        } catch (SQLException e) {


        }
    }

    public void dropAllTables() {

    }

    public static void main(String[] args) throws SQLException {

        CoursesManager coursesManager = new CoursesManager();
        coursesManager.createCoursesTable();
        coursesManager.createAttendanceListTable();
        coursesManager.createStudentTable();
    }
}
