package pl.sda.jdbc.starter;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {
    private static Logger logger = LoggerFactory.getLogger(ConnectionFactory.class);
    private static DataSource dataSource;
    private String fileName;

    public ConnectionFactory(String fileName) throws SQLException {
        Properties dataBaseProperties = getDataBaseProperties(fileName);
        dataSource = getDataSource(dataBaseProperties);
    }

    private DataSource getDataSource(Properties dataBaseProperties) throws SQLException {

        String serverName = dataBaseProperties.getProperty("pl.sda.jdbc.db.server");
        String DB_NAME = dataBaseProperties.getProperty("pl.sda.jdbc.db.name");
        String DB_USER = dataBaseProperties.getProperty("pl.sda.jdbc.db.user");
        String DB_PASSWORD = dataBaseProperties.getProperty("pl.sda.jdbc.db.password");
        Integer DB_PORT = Integer.parseInt(dataBaseProperties.getProperty("pl.sda.jdbc.db.port"));

        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setServerName(serverName);
        mysqlDataSource.setDatabaseName(DB_NAME);
        mysqlDataSource.setUser(DB_USER);
        mysqlDataSource.setPassword(DB_PASSWORD);
        mysqlDataSource.setPort(DB_PORT);
        mysqlDataSource.setServerTimezone("Europe/Warsaw");
        mysqlDataSource.setUseSSL(false);

        return mysqlDataSource;
    }

    public ConnectionFactory() throws SQLException {
        this("database.properties");
    }


    private Properties getDataBaseProperties(String filename) {
        Properties properties = new Properties();
        try {
            /*
             * Pobieramy zawartość pliku za pomocą classloadera, plik musi znajdować się w katalogu ustawionym w CLASSPATH
             */
            InputStream propertiesStream = ConnectionFactory.class.getResourceAsStream(filename);
            if (propertiesStream == null) {
                throw new IllegalArgumentException("Can't find file: " + filename);
            }
            /*
             * Pobieramy dane z pliku i umieszczamy w obiekcie klasy Properties
             */
            properties.load(propertiesStream);
        } catch (IOException e) {
            logger.error("Error during fetching properties for database", e);
            return null;
        }
        return properties;
    }

    public Connection getConnection() throws SQLException {
        if (dataSource == null) {
            throw new IllegalStateException("Data source is not created yet");
        }
        return dataSource.getConnection();
    }

    public static void main(String[] args) throws SQLException {
    ConnectionFactory connectionFactory = new ConnectionFactory();
    try(Connection connection= connectionFactory.getConnection()) {
        logger.info("Connection " + connection);
        logger.info("Database name " + connection.getCatalog());
    } catch (SQLException e){
        logger.error("Error during using connection" + e);
    }
    }



}